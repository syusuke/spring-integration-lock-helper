package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.LockOperation;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.annotation.SpringCacheAnnotationParser
 */

public class SpringLockAnnotationParser implements LockAnnotationParser, Serializable {
    private static final Set<Class<? extends Annotation>> LOCK_OPERATION_ANNOTATIONS = new LinkedHashSet<>(4);

    static {
        LOCK_OPERATION_ANNOTATIONS.add(Lock.class);
        LOCK_OPERATION_ANNOTATIONS.add(Locks.class);
    }

    @Override
    public boolean isCandidateClass(Class<?> targetClass) {
        return AnnotationUtils.isCandidateClass(targetClass, LOCK_OPERATION_ANNOTATIONS);
    }

    @Nullable
    @Override
    public Collection<LockOperation> parseLockAnnotations(Class<?> type) {
        return parseLockAnnotation(type);
    }

    @Nullable
    @Override
    public Collection<LockOperation> parseLockAnnotations(Method method) {
        return parseLockAnnotation(method);
    }

    @Nullable
    private Collection<LockOperation> parseLockAnnotation(AnnotatedElement ae) {
        Collection<? extends Annotation> anns = AnnotatedElementUtils.getAllMergedAnnotations(ae, LOCK_OPERATION_ANNOTATIONS);
        if (anns.isEmpty()) {
            return null;
        }
        final Collection<LockOperation> ops = new ArrayList<>(1);
        anns.stream().filter(ann -> ann instanceof Lock).forEach(ann -> ops.add(parseLockAnnotation(ae, (Lock) ann)));
        anns.stream().filter(ann -> ann instanceof Locks).forEach(ann -> ops.addAll(parseLocksAnnotation(ae, (Locks) ann)));
        return ops;
    }


    /**
     * 解析 @Lock 注解变成 LockOperation 对象
     *
     * @param ae
     * @param lock
     * @return
     */
    private LockOperation parseLockAnnotation(AnnotatedElement ae, Lock lock) {
        LockOperation.Builder builder = new LockOperation.Builder();
        LockOperation operation = builder
                .setName(ae.toString())
                .setLockKey(lock.value())
                .setUseTryLock(lock.useTryLock())
                .setTryLockTimeout(lock.tryLockTimeout())
                .setLockRegistry(lock.lockRegistry())
                .setKeyGenerator(lock.keyGenerator())
                .build();
        validateLockOperation(ae, operation);
        return operation;
    }

    private void validateLockOperation(AnnotatedElement ae, LockOperation operation) {
        if (StringUtils.hasText(operation.getLockKey()) && StringUtils.hasText(operation.getKeyGenerator())) {
            throw new IllegalStateException("Invalid lock annotation configuration on '" +
                    ae.toString() + "'. Both 'lockKey' and 'keyGenerator' attributes have been set. " +
                    "These attributes are mutually exclusive: either set the SpEL expression used to" +
                    "compute the key at runtime or set the name of the KeyGenerator bean to use.");
        }
        //TODO 验证
    }


    private List<LockOperation> parseLocksAnnotation(AnnotatedElement ae, Locks locks) {
        List<LockOperation> lockOperations = new ArrayList<>(locks.value().length);
        for (Lock lock : locks.value()) {
            lockOperations.add(parseLockAnnotation(ae, lock));
        }
        return lockOperations;
    }


}
