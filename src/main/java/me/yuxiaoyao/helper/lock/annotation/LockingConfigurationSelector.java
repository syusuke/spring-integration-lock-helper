package me.yuxiaoyao.helper.lock.annotation;

import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.AdviceModeImportSelector;
import org.springframework.context.annotation.AutoProxyRegistrar;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kerryzhang on 20/11/25
 * @see org.springframework.cache.annotation.CachingConfigurationSelector
 */

public class LockingConfigurationSelector extends AdviceModeImportSelector<EnableLocking> {


    @Override
    protected String[] selectImports(AdviceMode adviceMode) {
        switch (adviceMode) {
            case PROXY:
                return getProxyImports();
            case ASPECTJ:
                return getAspectJImports();
            default:
                return null;
        }
    }

    private String[] getProxyImports() {
        List<String> result = new ArrayList<>(3);
        result.add(AutoProxyRegistrar.class.getName());
        result.add(ProxyLockingConfiguration.class.getName());
        return StringUtils.toStringArray(result);
    }

    private String[] getAspectJImports() {
        //List<String> result = new ArrayList<>(2);
        //result.add(CACHE_ASPECT_CONFIGURATION_CLASS_NAME);
        //if (jsr107Present && jcacheImplPresent) {
        //    result.add(JCACHE_ASPECT_CONFIGURATION_CLASS_NAME);
        //}
        //return StringUtils.toStringArray(result);
        return new String[0];
    }


}
