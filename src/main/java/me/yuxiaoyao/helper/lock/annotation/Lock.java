package me.yuxiaoyao.helper.lock.annotation;

import java.lang.annotation.*;

/**
 * @author kerryzhang on 2020/11/18
 */


@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Lock {
    /**
     * lock key
     *
     * @return
     */
    String value();


    /**
     * when return true means lock.tryLock()
     * when return false means lock.lock()
     *
     * @return
     */
    boolean useTryLock() default false;

    /**
     * mills
     *
     * @return
     */
    long tryLockTimeout() default -1;

    /**
     * custom LockRegistry default "" use Spring Primary {@link org.springframework.integration.support.locks.LockRegistry} Bean
     *
     * @return LockRegistry beanName
     */
    String lockRegistry() default "";

    /**
     * lock key generator {@link me.yuxiaoyao.helper.lock.interceptor.KeyGenerator} beanName
     *
     * @return
     */
    String keyGenerator() default "";

}
