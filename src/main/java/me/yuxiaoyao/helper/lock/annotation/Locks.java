package me.yuxiaoyao.helper.lock.annotation;

import java.lang.annotation.*;

/**
 * @author kerryzhang on 2020/11/18
 */


@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Locks {

    /**
     * lock list
     *
     * @return
     */
    Lock[] value() default {};

}
