package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.AbstractFallbackLockOperationSource;
import me.yuxiaoyao.helper.lock.interceptor.LockOperation;
import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.annotation.AnnotationCacheOperationSource
 */

public class AnnotationLockOperationSource extends AbstractFallbackLockOperationSource implements Serializable {

    private final boolean publicMethodsOnly;
    private final Set<LockAnnotationParser> annotationParsers;


    public AnnotationLockOperationSource() {
        this(true);
    }

    public AnnotationLockOperationSource(boolean publicMethodsOnly) {
        this.publicMethodsOnly = publicMethodsOnly;
        this.annotationParsers = Collections.singleton(new SpringLockAnnotationParser());
    }

    public AnnotationLockOperationSource(boolean publicMethodsOnly, Set<LockAnnotationParser> annotationParsers) {
        this.publicMethodsOnly = publicMethodsOnly;
        this.annotationParsers = annotationParsers;
    }

    @Override
    public boolean isCandidateClass(Class<?> targetClass) {
        for (LockAnnotationParser parser : this.annotationParsers) {
            if (parser.isCandidateClass(targetClass)) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected Collection<LockOperation> findLockOperations(Class<?> clazz) {
        return determineLockOperations(parser -> parser.parseLockAnnotations(clazz));
    }

    @Override
    protected Collection<LockOperation> findLockOperations(Method method) {
        return determineLockOperations(parser -> parser.parseLockAnnotations(method));
    }

    @Nullable
    protected Collection<LockOperation> determineLockOperations(LockOperationProvider provider) {
        Collection<LockOperation> ops = null;
        for (LockAnnotationParser parser : this.annotationParsers) {
            Collection<LockOperation> annOps = provider.getLockOperations(parser);
            if (annOps != null) {
                if (ops == null) {
                    ops = annOps;
                } else {
                    Collection<LockOperation> combined = new ArrayList<>(ops.size() + annOps.size());
                    combined.addAll(ops);
                    combined.addAll(annOps);
                    ops = combined;
                }
            }
        }
        return ops;
    }


    @Override
    protected boolean allowPublicMethodsOnly() {
        return this.publicMethodsOnly;
    }

    @Override
    public boolean equals(@Nullable Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof AnnotationLockOperationSource)) {
            return false;
        }
        AnnotationLockOperationSource otherCos = (AnnotationLockOperationSource) other;
        return (this.annotationParsers.equals(otherCos.annotationParsers) &&
                this.publicMethodsOnly == otherCos.publicMethodsOnly);
    }

    @Override
    public int hashCode() {
        return this.annotationParsers.hashCode();
    }

    /**
     * org.springframework.cache.annotation.AnnotationCacheOperationSource.CacheOperationProvider
     */
    @FunctionalInterface
    protected interface LockOperationProvider {
        /**
         * get
         *
         * @param parser
         * @return
         */
        @Nullable
        Collection<LockOperation> getLockOperations(LockAnnotationParser parser);
    }

}
