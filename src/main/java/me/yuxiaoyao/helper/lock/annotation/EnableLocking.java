package me.yuxiaoyao.helper.lock.annotation;

import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.integration.support.locks.LockRegistry;

import java.lang.annotation.*;

/**
 * @author kerryzhang on 2020/11/18
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(LockingConfigurationSelector.class)
@ComponentScan(basePackages = {"me.yuxiaoyao.helper.lock.annotation", "me.yuxiaoyao.helper.lock.interceptor"})
@ConditionalOnBean(LockRegistry.class)
public @interface EnableLocking {
    AdviceMode mode() default AdviceMode.PROXY;
}
