package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.KeyGenerator;
import me.yuxiaoyao.helper.lock.interceptor.LockErrorHandler;
import org.springframework.integration.support.locks.LockRegistry;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.annotation.CachingConfigurerSupport
 */

public class LockingConfigurerSupport implements LockingConfigurer {
    @Override
    public LockRegistry lockRegistry() {
        return null;
    }

    @Override
    public KeyGenerator keyGenerator() {
        return null;
    }

    @Override
    public LockErrorHandler errorHandler() {
        return null;
    }
}
