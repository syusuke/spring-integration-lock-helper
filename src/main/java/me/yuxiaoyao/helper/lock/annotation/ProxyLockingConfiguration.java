package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.BeanFactoryLockOperationSourceAdvisor;
import me.yuxiaoyao.helper.lock.interceptor.LockInterceptor;
import me.yuxiaoyao.helper.lock.interceptor.LockOperationSource;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Role;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.annotation.ProxyCachingConfiguration
 */


@Configuration(proxyBeanMethods = false)
@Role(BeanDefinition.ROLE_INFRASTRUCTURE)
public class ProxyLockingConfiguration extends AbstractLockingConfiguration {

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public LockOperationSource lockOperationSource() {
        return new AnnotationLockOperationSource();
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public LockInterceptor lockInterceptor(LockOperationSource lockOperationSource) {
        LockInterceptor lockInterceptor = new LockInterceptor();
        lockInterceptor.setLockOperationSource(lockOperationSource);
        return lockInterceptor;
    }

    @Bean
    @Role(BeanDefinition.ROLE_INFRASTRUCTURE)
    public BeanFactoryLockOperationSourceAdvisor lockAdvisor(LockInterceptor lockInterceptor,
                                                             LockOperationSource lockOperationSource) {
        BeanFactoryLockOperationSourceAdvisor pointcutAdvisor = new BeanFactoryLockOperationSourceAdvisor();
        pointcutAdvisor.setLockOperationSource(lockOperationSource);
        pointcutAdvisor.setAdvice(lockInterceptor);
        return pointcutAdvisor;
    }
}
