package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.KeyGenerator;
import me.yuxiaoyao.helper.lock.interceptor.LockErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.function.Supplier;

/**
 * @author kerryzhang on 2020/11/18
 */

@Configuration(proxyBeanMethods = false)
public abstract class AbstractLockingConfiguration implements ImportAware {
    @Nullable
    protected AnnotationAttributes enableLocking;
    @Nullable
    protected Supplier<LockRegistry> lockRegistry;
    @Nullable
    protected Supplier<KeyGenerator> keyGenerator;
    @Nullable
    protected Supplier<LockErrorHandler> errorHandler;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        this.enableLocking = AnnotationAttributes.fromMap(
                importMetadata.getAnnotationAttributes(EnableLocking.class.getName(), false));
        if (this.enableLocking == null) {
            throw new IllegalArgumentException(
                    "@EnableLocking is not present on importing class " + importMetadata.getClassName());
        }
    }

    @Autowired(required = false)
    void setConfigurers(Collection<LockingConfigurer> configurers) {
        if (CollectionUtils.isEmpty(configurers)) {
            return;
        }
        if (configurers.size() > 1) {
            throw new IllegalStateException(configurers.size() + " implementations of " +
                    "LockingConfigurer were found when only 1 was expected. " +
                    "Refactor the configuration such that LockingConfigurer is " +
                    "implemented only once or not at all.");
        }
        LockingConfigurer configurer = configurers.iterator().next();
        useLockingConfigurer(configurer);
    }

    /**
     * Extract the configuration from the nominated {@link LockingConfigurer}.
     */
    protected void useLockingConfigurer(LockingConfigurer config) {
        this.lockRegistry = config::lockRegistry;
        this.keyGenerator = config::keyGenerator;
        this.errorHandler = config::errorHandler;
    }
}
