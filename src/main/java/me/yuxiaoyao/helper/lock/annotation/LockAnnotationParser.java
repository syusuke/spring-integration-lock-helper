package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.LockOperation;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.annotation.CacheAnnotationParser
 */

public interface LockAnnotationParser {

    /**
     * is match
     *
     * @param targetClass
     * @return
     */
    default boolean isCandidateClass(Class<?> targetClass) {
        return true;
    }


    /**
     * get type all lock annotations
     *
     * @param type
     * @return
     */
    @Nullable
    Collection<LockOperation> parseLockAnnotations(Class<?> type);

    /**
     * get method all lock annotations
     *
     * @param method
     * @return
     */
    @Nullable
    Collection<LockOperation> parseLockAnnotations(Method method);
}
