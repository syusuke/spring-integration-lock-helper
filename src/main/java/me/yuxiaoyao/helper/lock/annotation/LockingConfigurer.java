package me.yuxiaoyao.helper.lock.annotation;

import me.yuxiaoyao.helper.lock.interceptor.KeyGenerator;
import me.yuxiaoyao.helper.lock.interceptor.LockErrorHandler;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.lang.Nullable;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.annotation.CachingConfigurer
 */

public interface LockingConfigurer {
    /**
     * lock registry
     *
     * @return
     */
    @Nullable
    LockRegistry lockRegistry();

    /**
     * key gen
     *
     * @return
     */
    @Nullable
    KeyGenerator keyGenerator();

    /**
     * error handler
     *
     * @return
     */
    LockErrorHandler errorHandler();
}
