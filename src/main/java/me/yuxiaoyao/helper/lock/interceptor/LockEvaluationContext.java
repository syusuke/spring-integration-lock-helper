package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

/**
 * @author kerryzhang on 2020/11/19
 * @see org.springframework.cache.interceptor.CacheEvaluationContext
 * <p>
 * 直接用 {@link MethodBasedEvaluationContext} 替换就可以的
 */


@Deprecated
class LockEvaluationContext extends MethodBasedEvaluationContext {
    private final Set<String> unavailableVariables = new HashSet<>(1);

    public LockEvaluationContext(Object rootObject, Method method, Object[] arguments, ParameterNameDiscoverer parameterNameDiscoverer) {
        super(rootObject, method, arguments, parameterNameDiscoverer);
    }

    public void addUnavailableVariable(String name) {
        this.unavailableVariables.add(name);
    }

    @Override
    @Nullable
    public Object lookupVariable(String name) {
        if (this.unavailableVariables.contains(name)) {
            throw new VariableNotAvailableException(name);
        }
        return super.lookupVariable(name);
    }
}
