package me.yuxiaoyao.helper.lock.interceptor;

import me.yuxiaoyao.helper.lock.expression.LockExpressionEvaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author kerryzhang on 2020/11/19
 * @see org.springframework.cache.interceptor.CacheOperationExpressionEvaluator
 * <p>
 * use in {@link LockAspectSupport#evaluator}
 */

class LockOperationExpressionEvaluator extends LockExpressionEvaluator {

    /**
     * SpEL表达式缓存
     */
    private final Map<ExpressionKey, Expression> keyCache = new ConcurrentHashMap<>(64);


    public EvaluationContext createEvaluationContext(Method method,
                                                     Object[] args,
                                                     Object target,
                                                     Class<?> targetClass,
                                                     Method targetMethod,
                                                     @Nullable BeanFactory beanFactory) {

        LockExpressionRootObject rootObject = new LockExpressionRootObject(method, args, target, targetClass);

        MethodBasedEvaluationContext evaluationContext = new MethodBasedEvaluationContext(
                rootObject, targetMethod, args, getParameterNameDiscoverer());
        if (beanFactory != null) {
            evaluationContext.setBeanResolver(new BeanFactoryResolver(beanFactory));
        }
        return evaluationContext;
    }

    /**
     * get lock key
     *
     * @param keyExpression
     * @param methodKey
     * @param evalContext
     * @return
     */
    @Nullable
    public Object lockKey(String keyExpression, AnnotatedElementKey methodKey, EvaluationContext evalContext) {
        Expression expression = getExpression(this.keyCache, methodKey, keyExpression);
        return expression.getValue(evalContext);
    }

    private static final Logger logger = LoggerFactory.getLogger(LockOperationExpressionEvaluator.class);

    void clear() {
        this.keyCache.clear();
    }

}
