package me.yuxiaoyao.helper.lock.interceptor;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.CacheErrorHandler
 */

public interface LockErrorHandler {
    /**
     * lock failed
     *
     * @param exception
     * @param key
     */
    void handleLockError(RuntimeException exception, Object key);
}
