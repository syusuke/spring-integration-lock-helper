package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.dao.CannotAcquireLockException;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.util.function.SingletonSupplier;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.AbstractCacheInvoker
 */

public abstract class AbstractLockInvoker {
    protected SingletonSupplier<LockErrorHandler> errorHandler;

    protected AbstractLockInvoker() {
        this.errorHandler = SingletonSupplier.of(SimpleLockErrorHandler::new);
    }

    protected AbstractLockInvoker(LockErrorHandler errorHandler) {
        this.errorHandler = SingletonSupplier.of(errorHandler);
    }

    public LockErrorHandler getErrorHandler() {
        return errorHandler.obtain();
    }

    public void setErrorHandler(LockErrorHandler errorHandler) {
        this.errorHandler = SingletonSupplier.of(errorHandler);
    }


    protected Object doWithLock(LockRegistry lockRegistry, Object lockKey, LockOperationInvoker invoker) {
        Lock lock = lockRegistry.obtain(lockKey);
        boolean lockState = false;
        try {
            //noinspection AlibabaLockShouldWithTryFinally
            lock.lock();
            lockState = true;
            return invoker.invoke();
        } catch (CannotAcquireLockException e) {
            getErrorHandler().handleLockError(e, lockKey);
            // If the exception is handled, return a cache miss
            return null;
        } finally {
            if (lockState) {
                lock.unlock();
            }
        }
    }

    protected Object doWithTryLock(LockRegistry lockRegistry, Object lockKey, long timeout, LockOperationInvoker invoker) {
        Lock lock = lockRegistry.obtain(lockKey);
        boolean lockState = false;
        try {
            if (timeout > 0) {
                lockState = lock.tryLock();
            } else {
                lockState = lock.tryLock(timeout, TimeUnit.MILLISECONDS);
            }
            return invoker.invoke();
        } catch (CannotAcquireLockException | InterruptedException e) {
            // wrapper exception
            getErrorHandler().handleLockError(new CannotAcquireLockException(e.getMessage(), e.getCause()), lockKey);
            // If the exception is handled, return a cache miss
            return null;
        } finally {
            if (lockState) {
                lock.unlock();
            }
        }
    }

}
