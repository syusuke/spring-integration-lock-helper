package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.lang.Nullable;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.CacheOperationInvoker
 */
@FunctionalInterface
public interface LockOperationInvoker {
    /**
     * Invoke the cache operation defined by this instance. Wraps any exception
     * that is thrown during the invocation in a {@link ThrowableWrapper}.
     *
     * @return the result of the operation
     * @throws ThrowableWrapper if an error occurred while invoking the operation
     */
    @Nullable
    Object invoke() throws ThrowableWrapper;


    /**
     * Wrap any exception thrown while invoking {@link #invoke()}.
     */
    @SuppressWarnings("serial")
    class ThrowableWrapper extends RuntimeException {

        private final Throwable original;

        public ThrowableWrapper(Throwable original) {
            super(original.getMessage(), original);
            this.original = original;
        }

        public Throwable getOriginal() {
            return this.original;
        }
    }

}
