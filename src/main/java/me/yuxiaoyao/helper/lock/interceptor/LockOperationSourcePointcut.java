package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.StaticMethodMatcherPointcut;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.lang.Nullable;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * @author kerryzhang on 20/11/25
 * @see org.springframework.cache.interceptor.CacheOperationSourcePointcut
 */

abstract class LockOperationSourcePointcut extends StaticMethodMatcherPointcut implements Serializable {


    protected LockOperationSourcePointcut() {
        setClassFilter(new LockOperationSourceClassFilter());
    }

    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        LockOperationSource lps = getLockOperationSource();
        return (lps != null && !CollectionUtils.isEmpty(lps.getLockOperations(method, targetClass)));
    }


    @Override
    public boolean equals(@Nullable Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof LockOperationSourcePointcut)) {
            return false;
        }
        LockOperationSourcePointcut otherPc = (LockOperationSourcePointcut) other;
        return ObjectUtils.nullSafeEquals(getLockOperationSource(), otherPc.getLockOperationSource());
    }

    @Override
    public int hashCode() {
        return LockOperationSourcePointcut.class.hashCode();
    }

    @Override
    public String toString() {
        return getClass().getName() + ": " + getLockOperationSource();
    }


    @Nullable
    protected abstract LockOperationSource getLockOperationSource();


    /**
     * @see org.springframework.cache.interceptor.CacheOperationSourcePointcut.CacheOperationSourceClassFilter
     */
    private class LockOperationSourceClassFilter implements ClassFilter {

        @Override
        public boolean matches(Class<?> clazz) {
            if (LockRegistry.class.isAssignableFrom(clazz)) {
                return false;
            }
            LockOperationSource lps = getLockOperationSource();
            return (lps == null || lps.isCandidateClass(clazz));
        }
    }

}
