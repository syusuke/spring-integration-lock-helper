package me.yuxiaoyao.helper.lock.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.AopProxyUtils;
import org.springframework.aop.support.AopUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.cglib.proxy.Proxy;
import org.springframework.context.expression.AnnotatedElementKey;
import org.springframework.core.BridgeMethodResolver;
import org.springframework.expression.EvaluationContext;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.lang.Nullable;
import org.springframework.util.*;
import org.springframework.util.function.SingletonSupplier;
import org.springframework.util.function.SupplierUtils;

import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.function.Supplier;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.CacheAspectSupport
 */

public abstract class LockAspectSupport extends AbstractLockInvoker implements BeanFactoryAware, InitializingBean, SmartInitializingSingleton {

    private static final Logger logger = LoggerFactory.getLogger(LockAspectSupport.class);

    private final Map<LockOperationCacheKey, LockOperationMetadata> metadataCache = new ConcurrentHashMap<>(1024);

    /**
     * SpEL 表达式解析器
     */
    private final LockOperationExpressionEvaluator evaluator = new LockOperationExpressionEvaluator();
    @Nullable
    private LockOperationSource lockOperationSource;

    private SingletonSupplier<KeyGenerator> keyGenerator = SingletonSupplier.of(SimpleKeyGenerator::new);

    @Nullable
    private SingletonSupplier<LockRegistry> lockRegistry;

    @Nullable
    private BeanFactory beanFactory;

    private boolean initialized = false;


    public void configure(@Nullable Supplier<LockErrorHandler> errorHandler,
                          @Nullable Supplier<KeyGenerator> keyGenerator,
                          @Nullable Supplier<LockRegistry> lockRegistry) {
        this.errorHandler = new SingletonSupplier<>(errorHandler, SimpleLockErrorHandler::new);
        this.keyGenerator = new SingletonSupplier<>(keyGenerator, SimpleKeyGenerator::new);
        // default null
        this.lockRegistry = SingletonSupplier.ofNullable(lockRegistry);
    }

    public void setLockOperationSource(@Nullable LockOperationSource lockOperationSource) {
        this.lockOperationSource = lockOperationSource;
    }

    @Nullable
    public LockOperationSource getLockOperationSource() {
        return lockOperationSource;
    }

    public void setKeyGenerator(KeyGenerator keyGenerator) {
        this.keyGenerator = SingletonSupplier.of(keyGenerator);
    }

    public KeyGenerator getKeyGenerator() {
        return keyGenerator.obtain();
    }

    public void setLockRegistry(LockRegistry lockRegistry) {
        this.lockRegistry = SingletonSupplier.ofNullable(lockRegistry);
    }

    @Nullable
    public LockRegistry getLockRegistry() {
        return SupplierUtils.resolve(this.lockRegistry);
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.state(getLockOperationSource() != null, "The 'lockOperationSource' property is required: " +
                "If there are no lock methods, then don't use a lock aspect.");
    }

    @Override
    public void afterSingletonsInstantiated() {
        if (getLockRegistry() == null) {
            // Lazily initialize cache resolver via default cache manager...
            Assert.state(this.beanFactory != null, "LockRegistry or BeanFactory must be set on cache aspect");
            try {
                setLockRegistry(this.beanFactory.getBean(LockRegistry.class));
            } catch (NoUniqueBeanDefinitionException ex) {
                throw new IllegalStateException("No LockRegistry specified, and no unique bean of type " +
                        "LockRegistry found. Mark one as primary or declare a specific LockRegistry to use.");
            } catch (NoSuchBeanDefinitionException ex) {
                throw new IllegalStateException("No LockRegistry specified, and no bean of type LockRegistry found. " +
                        "Register a LockRegistry bean or remove the @EnableLocking annotation from your configuration.");
            }
        }
        this.initialized = true;
    }

    protected String methodIdentification(Method method, Class<?> targetClass) {
        Method specificMethod = ClassUtils.getMostSpecificMethod(method, targetClass);
        return ClassUtils.getQualifiedMethodName(specificMethod);
    }

    protected LockOperationContext getOperationContext(LockOperation operation, Method method, Object[] args, Object target, Class<?> targetClass) {
        LockOperationMetadata metadata = getLockOperationMetadata(operation, method, targetClass);
        return new LockOperationContext(metadata, args, target);
    }

    /**
     * get current meta data
     *
     * @param operation
     * @param method
     * @param targetClass
     * @return
     */
    protected LockOperationMetadata getLockOperationMetadata(LockOperation operation, Method method, Class<?> targetClass) {
        // local cache key
        LockOperationCacheKey cacheKey = new LockOperationCacheKey(operation, method, targetClass);
        LockOperationMetadata metadata = this.metadataCache.get(cacheKey);
        if (metadata == null) {
            LockRegistry operationLockRegistry;
            if (StringUtils.hasText(operation.getLockRegistry())) {
                operationLockRegistry = getBean(operation.getLockRegistry(), LockRegistry.class);
            } else {
                operationLockRegistry = getLockRegistry();
            }
            metadata = new LockOperationMetadata(operation, method, targetClass, operationLockRegistry);
            this.metadataCache.put(cacheKey, metadata);
        }
        return metadata;
    }

    /**
     * use for @Lock(lockRegistry="customBeanName")
     *
     * @param beanName
     * @param expectedType
     * @param <T>
     * @return
     */
    protected <T> T getBean(String beanName, Class<T> expectedType) {
        if (this.beanFactory == null) {
            throw new IllegalStateException(
                    "BeanFactory must be set on cache aspect for " + expectedType.getSimpleName() + " retrieval");
        }
        return BeanFactoryAnnotationUtils.qualifiedBeanOfType(this.beanFactory, expectedType, beanName);
    }


    @Nullable
    protected Object execute(LockOperationInvoker invoker, Object target, Method method, Object[] args) {
        // Check whether aspect is enabled (to cope with cases where the AJ is pulled in automatically)
        if (this.initialized) {
            Class<?> targetClass = getTargetClass(target);
            LockOperationSource lockOperationSource = getLockOperationSource();
            if (lockOperationSource != null) {
                Collection<LockOperation> operations = lockOperationSource.getLockOperations(method, targetClass);
                if (!CollectionUtils.isEmpty(operations)) {
                    return execute(invoker, method, new LockOperationContexts(operations, method, args, target, targetClass));
                }
            }
        }
        return invoker.invoke();
    }


    @Nullable
    protected Object invokeOperation(LockOperationInvoker invoker) {
        return invoker.invoke();
    }

    private Class<?> getTargetClass(Object target) {
        return AopProxyUtils.ultimateTargetClass(target);
    }


    @Nullable
    protected Object execute(LockOperationInvoker invoker, Method method, LockOperationContexts contexts) {
        Collection<LockOperationContext> lockOperationContexts = contexts.get(LockOperation.class);
        if (lockOperationContexts.isEmpty()) {
            return invoker.invoke();
        }
        List<Lock> locks = new ArrayList<>(lockOperationContexts.size());
        // lock
        Object lockKey = null;
        try {
            for (LockOperationContext context : lockOperationContexts) {

                lockKey = context.generateKey();
                if (logger.isTraceEnabled()) {
                    logger.trace("lockKey = [{}]", lockKey);
                }
                Lock lock = context.metadata.lockRegistry.obtain(lockKey);
                //noinspection AlibabaLockShouldWithTryFinally
                lock.lock();
                locks.add(lock);
            }
        } catch (Throwable e) {
            getErrorHandler().handleLockError(
                    e instanceof RuntimeException ? (RuntimeException) e : new RuntimeException(e.getMessage(), e.getCause()), lockKey);
            // after handler
            return null;
        }

        try {
            // do
            return invoker.invoke();
        } finally {
            // unlock
            unLock(locks);
        }
    }

    private void unLock(List<Lock> locks) {
        for (int i = locks.size() - 1; i >= 0; i--) {
            try {
                locks.get(i).unlock();
            } catch (RuntimeException ignored) {
            }
        }
    }

    protected class LockOperationContext {
        private final LockOperationMetadata metadata;
        private final Object[] args;
        private final Object target;

        public LockOperationContext(LockOperationMetadata metadata, Object[] args, Object target) {
            this.metadata = metadata;
            this.args = args;
            this.target = target;
        }

        public LockOperation getOperation() {
            return this.metadata.operation;
        }


        public Object getTarget() {
            return this.target;
        }

        public Method getMethod() {
            return this.metadata.method;
        }

        public Object[] getArgs() {
            return this.args;
        }

        private Object[] extractArgs(Method method, Object[] args) {
            if (!method.isVarArgs()) {
                return args;
            }
            Object[] varArgs = ObjectUtils.toObjectArray(args[args.length - 1]);
            Object[] combinedArgs = new Object[args.length - 1 + varArgs.length];
            System.arraycopy(args, 0, combinedArgs, 0, args.length - 1);
            System.arraycopy(varArgs, 0, combinedArgs, args.length - 1, varArgs.length);
            return combinedArgs;
        }

        /**
         * gen lock key
         *
         * @return
         */
        @Nullable
        protected Object generateKey() {
            EvaluationContext evaluationContext = createEvaluationContext();
            return evaluator.lockKey(this.metadata.operation.getLockKey(), this.metadata.methodKey, evaluationContext);
        }

        private EvaluationContext createEvaluationContext() {
            return evaluator.createEvaluationContext(this.metadata.method, this.args,
                    this.target, this.metadata.targetClass, this.metadata.targetMethod, beanFactory);
        }
    }

    protected class LockOperationContexts {
        private final MultiValueMap<Class<? extends LockOperation>, LockOperationContext> contexts;

        public LockOperationContexts(Collection<? extends LockOperation> operations, Method method,
                                     Object[] args, Object target, Class<?> targetClass) {

            this.contexts = new LinkedMultiValueMap<>(operations.size());
            for (LockOperation op : operations) {
                this.contexts.add(op.getClass(), getOperationContext(op, method, args, target, targetClass));
            }
        }

        public Collection<LockOperationContext> get(Class<? extends LockOperation> operationClass) {
            Collection<LockOperationContext> result = this.contexts.get(operationClass);
            return (result != null ? result : Collections.emptyList());
        }
    }

    protected static class LockOperationMetadata {
        /**
         * @Lock annotation 注解解析后的对象
         */
        private final LockOperation operation;

        private final Method method;

        private final Class<?> targetClass;

        private final Method targetMethod;

        private final AnnotatedElementKey methodKey;
        /**
         * 解析之后Lock
         */
        private final LockRegistry lockRegistry;

        public LockOperationMetadata(LockOperation operation, Method method, Class<?> targetClass, LockRegistry lockRegistry) {
            this.operation = operation;
            this.method = BridgeMethodResolver.findBridgedMethod(method);
            this.targetClass = targetClass;
            this.targetMethod = (!Proxy.isProxyClass(targetClass) ? AopUtils.getMostSpecificMethod(method, targetClass) : this.method);
            this.methodKey = new AnnotatedElementKey(this.targetMethod, targetClass);
            this.lockRegistry = lockRegistry;
        }
    }

    /**
     * 缓存每个方法上的注解信息的KEY
     *
     * @see org.springframework.cache.interceptor.CacheAspectSupport.CacheOperationCacheKey
     */
    private static final class LockOperationCacheKey implements Comparable<LockOperationCacheKey> {
        private final LockOperation lockOperation;

        private final AnnotatedElementKey methodCacheKey;

        private LockOperationCacheKey(LockOperation lockOperation, Method method, Class<?> targetClass) {
            this.lockOperation = lockOperation;
            this.methodCacheKey = new AnnotatedElementKey(method, targetClass);
        }

        @Override
        public boolean equals(@Nullable Object other) {
            if (this == other) {
                return true;
            }
            if (!(other instanceof LockOperationCacheKey)) {
                return false;
            }
            LockOperationCacheKey otherKey = (LockOperationCacheKey) other;
            return (this.lockOperation.equals(otherKey.lockOperation) &&
                    this.methodCacheKey.equals(otherKey.methodCacheKey));
        }

        @Override
        public int hashCode() {
            return (this.lockOperation.hashCode() * 31 + this.methodCacheKey.hashCode());
        }

        @Override
        public String toString() {
            return this.lockOperation + " on " + this.methodCacheKey;
        }

        @Override
        public int compareTo(LockOperationCacheKey other) {
            int result = this.lockOperation.getName().compareTo(other.lockOperation.getName());
            if (result == 0) {
                result = this.methodCacheKey.compareTo(other.methodCacheKey);
            }
            return result;
        }
    }
}
