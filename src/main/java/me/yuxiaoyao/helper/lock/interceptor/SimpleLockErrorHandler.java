package me.yuxiaoyao.helper.lock.interceptor;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.SimpleCacheErrorHandler
 */

public class SimpleLockErrorHandler implements LockErrorHandler {
    @Override
    public void handleLockError(RuntimeException exception, Object key) {
        throw exception;
    }
}
