package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Collection;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.CacheOperationSource
 */

public interface LockOperationSource {
    /**
     * is
     *
     * @param targetClass
     * @return
     */
    default boolean isCandidateClass(Class<?> targetClass) {
        return true;
    }

    /**
     * get list
     *
     * @param method
     * @param targetClass
     * @return
     */

    @Nullable
    Collection<LockOperation> getLockOperations(Method method, @Nullable Class<?> targetClass);
}
