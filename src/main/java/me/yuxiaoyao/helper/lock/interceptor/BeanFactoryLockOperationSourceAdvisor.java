package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.AbstractBeanFactoryPointcutAdvisor;
import org.springframework.cache.interceptor.CacheOperationSource;
import org.springframework.lang.Nullable;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.BeanFactoryCacheOperationSourceAdvisor
 */

public class BeanFactoryLockOperationSourceAdvisor extends AbstractBeanFactoryPointcutAdvisor {

    @Nullable
    private LockOperationSource lockOperationSource;

    private final LockOperationSourcePointcut pointcut = new LockOperationSourcePointcut() {
        @Override
        @Nullable
        protected LockOperationSource getLockOperationSource() {
            return lockOperationSource;
        }
    };


    public void setLockOperationSource(@Nullable LockOperationSource lockOperationSource) {
        this.lockOperationSource = lockOperationSource;
    }

    public void setClassFilter(ClassFilter classFilter) {
        this.pointcut.setClassFilter(classFilter);
    }

    @Override
    public Pointcut getPointcut() {
        return this.pointcut;
    }
}