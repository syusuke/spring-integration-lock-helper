package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.lang.Nullable;

/**
 * @author kerryzhang on 2020/11/18
 * 缓存 @Lock 注解解析后的参数
 * @see org.springframework.cache.interceptor.CacheOperation
 */

public class LockOperation {
    private final String name;
    private final String lockKey;
    private final boolean useTryLock;
    private final long tryLockTimeout;
    private final String lockRegistry;
    private final String keyGenerator;
    private final String toString;


    public LockOperation(Builder builder) {
        this.name = builder.name;
        this.lockKey = builder.lockKey;
        this.useTryLock = builder.useTryLock;
        this.tryLockTimeout = builder.tryLockTimeout;
        this.lockRegistry = builder.lockRegistry;
        this.keyGenerator = builder.keyGenerator;
        this.toString = builder.getOperationDescription().toString();
    }

    public String getName() {
        return name;
    }

    public String getLockKey() {
        return lockKey;
    }

    public boolean isUseTryLock() {
        return useTryLock;
    }

    public long getTryLockTimeout() {
        return tryLockTimeout;
    }

    public String getLockRegistry() {
        return lockRegistry;
    }

    public String getKeyGenerator() {
        return keyGenerator;
    }

    /**
     * This implementation compares the {@code toString()} results.
     *
     * @see #toString()
     */
    @Override
    public boolean equals(@Nullable Object other) {
        return (other instanceof LockOperation && toString().equals(other.toString()));
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public final String toString() {
        return this.toString;
    }


    /**
     * @see org.springframework.cache.interceptor.CacheOperation.Builder
     */
    public static class Builder {
        private String name = "";
        private String lockKey;
        private boolean useTryLock = false;
        private long tryLockTimeout = -1;
        private String lockRegistry;
        private String keyGenerator;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setLockKey(String lockKey) {
            this.lockKey = lockKey;
            return this;
        }

        public Builder setUseTryLock(boolean useTryLock) {
            this.useTryLock = useTryLock;
            return this;
        }

        public Builder setTryLockTimeout(long tryLockTimeout) {
            this.tryLockTimeout = tryLockTimeout;
            return this;
        }

        public Builder setLockRegistry(String lockRegistry) {
            this.lockRegistry = lockRegistry;
            return this;
        }

        public Builder setKeyGenerator(String keyGenerator) {
            this.keyGenerator = keyGenerator;
            return this;
        }

        protected StringBuilder getOperationDescription() {
            StringBuilder result = new StringBuilder(getClass().getSimpleName());
            result.append("[").append(this.name);
            result.append("] lockKey='").append(this.lockKey);
            result.append("' | useTryLock='").append(this.useTryLock);
            result.append("' | tryLockTimeout='").append(this.tryLockTimeout);
            result.append("' | lockRegistry='").append(this.lockRegistry);
            result.append("' | keyGenerator='").append(this.keyGenerator).append("'");
            return result;
        }

        public LockOperation build() {
            return new LockOperation(this);
        }
    }
}
