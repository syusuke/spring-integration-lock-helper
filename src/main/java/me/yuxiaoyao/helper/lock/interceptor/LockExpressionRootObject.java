package me.yuxiaoyao.helper.lock.interceptor;

import java.lang.reflect.Method;

/**
 * @author kerryzhang on 2020/11/19
 * <p>
 * 这里的 getter 方法很重要,决定了参数的解析
 * <p>
 * @see org.springframework.cache.interceptor.CacheExpressionRootObject
 */

class LockExpressionRootObject {
    private final Method method;

    private final Object[] args;

    private final Object target;

    private final Class<?> targetClass;


    public LockExpressionRootObject(Method method, Object[] args, Object target, Class<?> targetClass) {
        this.method = method;
        this.args = args;
        this.target = target;
        this.targetClass = targetClass;
    }

    public Method getMethod() {
        return method;
    }

    public String getMethodName() {
        return method.getName();
    }


    public Object[] getArgs() {
        return args;
    }

    public Object getTarget() {
        return target;
    }

    public Class<?> getTargetClass() {
        return targetClass;
    }
}
