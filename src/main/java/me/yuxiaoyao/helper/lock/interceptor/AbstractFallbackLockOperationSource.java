package me.yuxiaoyao.helper.lock.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.support.AopUtils;
import org.springframework.core.MethodClassKey;
import org.springframework.lang.Nullable;
import org.springframework.util.ClassUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.AbstractFallbackCacheOperationSource
 */

public abstract class AbstractFallbackLockOperationSource implements LockOperationSource {
    protected static final Logger logger = LoggerFactory.getLogger(AbstractFallbackLockOperationSource.class);
    /**
     * 避免重复对一些没有注解的方法进行解析
     */
    private static final Collection<LockOperation> NULL_LOCKING_ATTRIBUTE = Collections.emptyList();

    /**
     * 方法解析过的注解缓存
     */
    private final Map<Object, Collection<LockOperation>> attributeCache = new ConcurrentHashMap<>(1024);


    @Override
    public Collection<LockOperation> getLockOperations(Method method, Class<?> targetClass) {
        if (method.getDeclaringClass() == Object.class) {
            return null;
        }
        Object cacheKey = getCacheKey(method, targetClass);
        Collection<LockOperation> cached = this.attributeCache.get(cacheKey);
        if (cached != null) {
            return (cached != NULL_LOCKING_ATTRIBUTE ? cached : null);
        } else {
            Collection<LockOperation> lockOps = computeLockOperations(method, targetClass);
            if (lockOps != null) {
                if (logger.isTraceEnabled()) {
                    logger.trace("Adding cacheable method '" + method.getName() + "' with attribute: " + lockOps);
                }
                this.attributeCache.put(cacheKey, lockOps);
            } else {
                this.attributeCache.put(cacheKey, NULL_LOCKING_ATTRIBUTE);
            }
            return lockOps;
        }
    }


    /**
     * method + class 唯一Key
     *
     * @param method
     * @param targetClass
     * @return
     */
    protected Object getCacheKey(Method method, @Nullable Class<?> targetClass) {
        return new MethodClassKey(method, targetClass);
    }

    @Nullable
    private Collection<LockOperation> computeLockOperations(Method method, @Nullable Class<?> targetClass) {
        // Don't allow no-public methods as required.
        if (allowPublicMethodsOnly() && !Modifier.isPublic(method.getModifiers())) {
            return null;
        }

        // The method may be on an interface, but we need attributes from the target class.
        // If the target class is null, the method will be unchanged.
        Method specificMethod = AopUtils.getMostSpecificMethod(method, targetClass);

        // First try is the method in the target class.
        Collection<LockOperation> opDef = findLockOperations(specificMethod);
        if (opDef != null) {
            return opDef;
        }

        // Second try is the caching operation on the target class.
        opDef = findLockOperations(specificMethod.getDeclaringClass());
        if (opDef != null && ClassUtils.isUserLevelMethod(method)) {
            return opDef;
        }

        if (specificMethod != method) {
            // Fallback is to look at the original method.
            opDef = findLockOperations(method);
            if (opDef != null) {
                return opDef;
            }
            // Last fallback is the class of the original method.
            opDef = findLockOperations(method.getDeclaringClass());
            if (opDef != null && ClassUtils.isUserLevelMethod(method)) {
                return opDef;
            }
        }

        return null;
    }

    /**
     * parser and find
     *
     * @param clazz
     * @return
     */
    @Nullable
    protected abstract Collection<LockOperation> findLockOperations(Class<?> clazz);

    /**
     * parser and find
     *
     * @param method
     * @return
     */
    @Nullable
    protected abstract Collection<LockOperation> findLockOperations(Method method);

    protected boolean allowPublicMethodsOnly() {
        return false;
    }

}
