package me.yuxiaoyao.helper.lock.interceptor;

import org.springframework.expression.EvaluationException;

/**
 * @author kerryzhang on 2020/11/19
 * @see org.springframework.cache.interceptor.VariableNotAvailableException
 */

class VariableNotAvailableException extends EvaluationException {
    private final String name;


    public VariableNotAvailableException(String name) {
        super("Variable not available");
        this.name = name;
    }


    public final String getName() {
        return this.name;
    }
}
