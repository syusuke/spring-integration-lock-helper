package me.yuxiaoyao.helper.lock.interceptor;

import java.lang.reflect.Method;

/**
 * @author kerryzhang on 2020/11/18
 * @see org.springframework.cache.interceptor.KeyGenerator
 */

@FunctionalInterface
public interface KeyGenerator {
    /**
     * 生成KEY
     *
     * @param target
     * @param method
     * @param params
     * @return
     */
    Object generate(Object target, Method method, Object... params);
}
